
### [EVALUATION] - ICDAR2017/2019 Competition on Post-OCR Text Correction 
Competition websites: https://l3i.univ-larochelle.fr/ICDAR2017PostOCR and https://l3i.univ-larochelle.fr/ICDAR2019PostOCR

Copy from previous Git repository: https://git.univ-lr.fr/gchiro01/icdar2017/

#### Sample data
- **./dataset_sample**: Directory that follows the same structure as the ICDAR2017 Post-OCR Cor. corpus
- **./inputErrCor_sample.json**: Example of formatted error/correction results corresponding to the dataset_sample

**Warning:** The JSON has to be encoded in UTF-8 to support accents. Format is détailed on:
https://sites.google.com/view/icdar2017-postcorrectionocr/evaluation (also in ./images/format.png)


#### Run the code
Try **./evalTool_ICDAR2017.py** on your own OCR correction results :
`$ python evalTool_ICDAR2017.py "./dataset_sample" "./inputErrCor_sample.json" "./output_eval.csv"`

This code will be used to compute metrics and ranks of the participants.
 
_Tested on Python 2.7 and 3.6_
  
  
#### Outputs
Per file :
- **NbTokens**: Number of non-ignored tokens considered for normalization in Task 1)
- **NbConsideredSymbols**: Number of non-ignored symbols for normalization in Task 2)
- **NbErroneousTokens**: Number of erroneous tokens, obtained by comparing aligned GS and OCR.
- **T1_Fmesure** (along with T1_Precision, T1_Recall): Metric used for ranking participants on **Task 1)**
- **T2_AvgLVDistCorrected** (along with T2_AvgLVDistOriginal for comparison): Metric used for ranking participants on **Task 2)**

Global scores for:
- **Task 1)**: will be an average of all **T1_Fmesure** weighted by **NbTokens**.
- **Task 2)**: will be an average of all **T2_AvgLVDistCorrected** weighted by **NbConsideredSymbols**.


#### Report issues
Please report issues/suggestions at guillaume.chiron(at)univ-lr.fr

**Disclaimer:** the evaluation code is likely to evolve, so please be sure to have the latest version up-to-date.


#### Contact
- christophe.rigaud(at)univ-lr.fr
- mickael.coustaty(at)univ-lr.fr
- antoine.doucet(at)univ-lr.fr

#### License
<a href="http://creativecommons.org/publicdomain/zero/1.0/"><img src="https://camo.githubusercontent.com/4df6de8c11e31c357bf955b12ab8c55f55c48823/68747470733a2f2f6c6963656e7365627574746f6e732e6e65742f702f7a65726f2f312e302f38387833312e706e67" alt="CC0" data-canonical-src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="max-width:100%;"></a> **CC0**
